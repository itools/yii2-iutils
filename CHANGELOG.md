yii2-iUtils Change Log
==========================================
v1.1.3
-----------------------------
- 修复已知的 bug

v1.1.0
-----------------------------
 - Service 类静态方法`newInstance`返回结果注释由`obeject` 改为 `static`；
 - 修复CurlHelper bug；
 - 修复`before action`里面使用`$this->error`报`Response content must not be an array` bug；

v1.0.200313, 2020.03.13
-----------------------------
- model基类支持软删除


v1.0, 2020.03.04
-----------------------------
- Initial release
# iUtils
框架基础软件包，包括错误管理，缓存管理，统一调用关系。底层统一错误捕获，数据统一返回格式，基类封装常用方法和请求校验，工具类提供快速校验、数组、字符串等操作，提高开发效率。

### 安装方法
------------
```
composer require mingyuanyun/yii2-iutils
```

### 目录结构
------------
--src 源代码  
&emsp;&emsp;|--connfig 提供快速的数据库配置表数据读取  
&emsp;&emsp;|--doc 文档介绍  
&emsp;&emsp;|--exception 常用异常类  
&emsp;&emsp;|--helper 实用工具辅助助手  
&emsp;&emsp;|--security 安全相关类,包括密码生成等  
&emsp;&emsp;|--validata 校验相关类  
&emsp;&emsp;|--yii2 提供与yii2相关的controller、service和model基类等   
&emsp;&emsp;|--Cacher.php 缓存代理,继承该类后提供缓存key统一管理  
&emsp;&emsp;|--Error.php 错误码和信息统一管理,继承该类后提供统一的错误管理  

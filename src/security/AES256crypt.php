<?php

namespace iUtils\security;

use iUtils\validate\Validator;

/**
 * 对称加密扩展类
 */
class AES256crypt
{
    /**
     * 密钥
     * @var string
     */
    private $secreyKey = '';

    /**
     * 设置密钥,不能少于8位,至少有一个大写、一个小写、一个特殊字母、一个数字构成
     * @param string $key
     * @throws \Exception
     */
    public function setKey($key)
    {
        if (!Validator::isSafeKey($key, 8, 1, 1, 1, 1)) {
            throw new \Exception('密钥不能少于8位,至少有一个大写、一个小写、一个特殊字母、一个数字构成');
        }
        
        $this->secreyKey = $key;
        return $this;
    }
    
    /**
     * 加密
     * @param sting $message
     */
    public function encrypt($message)
    {
        $data['iv'] = base64_encode(substr(sha1($this->secreyKey), 0, 16));
        $data['value'] = openssl_encrypt($message, 'AES-256-CBC', $this->secreyKey, 0, base64_decode($data['iv']));
        $encrypt=base64_encode(json_encode($data));
        return $encrypt;
    }
    
    /**
     * 解密
     * @param string $message
     */
    public function decrypt($message)
    {
        $encryptData = json_decode(base64_decode($message), true);
        $iv = base64_decode($encryptData['iv']);
        $decryptData = openssl_decrypt($encryptData['value'], 'AES-256-CBC', $this->secreyKey, 0, $iv);
        return $decryptData;
    }
}

<?php

namespace iUtils\security;

/**
 * 密码类
 *
 * @author ray
 */
class Password
{
    /**
     * 默认盐值
     */
    const DEFAULT_SALT = 'TzJyovzt#GZ7ymIvobBe@pYU46Z8ZKPqVW';

    /**
     * 获取一个安全的随机密码
     * @param string $type alnum, alpha, hexdec, numeric, nozero, distinct
     * @param int $length
     * @return string
     */
    public static function createRandom($type = 'alnum', $length = 10)
    {
        return \iUtils\StringHelper::random($type, $length);
    }
    
    /**
     * 密码加密
     * @param string $password 明文
     * @param string $algo 算法,aes, sha1
     * @param string $salt 盐值
     * @return string
     * @throws \InvalidArgumentException
     */
    public static function encrypt($password, $algo, $salt = '')
    {
        $salt = $salt ?: static::DEFAULT_SALT;
        switch (strtolower($algo)) {
            case 'aes':
                $encrypt = (new AES256crypt())->setKey($salt)->encrypt($password);
                break;
            case 'sha1':
                $salt = base64_encode(substr(sha1($salt), 0, 16));
                $encrypt = sha1($password . $salt);
                break;
            default :
                throw new \InvalidArgumentException("暂不支持{$algo}算法");
        }
        
        return $encrypt;
    }
    
    /**
     * 解密密码
     * @param string $encrypt 密文
     * @param string $algo 算法,ae
     * @param string $salt 盐值
     * @return string
     * @throws \InvalidArgumentException
     */
    public static function decrypt($encrypt, $algo, $salt = '')
    {
        $salt = $salt ?: static::DEFAULT_SALT;
        switch (strtolower($algo)) {
            case 'aes':
                $decrypt = (new AES256crypt())->setKey($salt)->decrypt($encrypt);
                break;
            default :
                throw new \InvalidArgumentException("暂不支持{$algo}算法");
        }
        
        return $decrypt;
    }
}

<?php

namespace iUtils;

/**
 * 缓存器,统一管理Key
 * 
 * 子类可继承该类并扩展自己的KEY常量
 *
 * @author ray
 */
class Cacher
{
    /**
     * 全局配置,所有的键采用:分段来隔离
     */
    const KEY_GLOBAL_CONFIG = "global:config:%s";
    
    /**
     * 获取具体的缓存key,参数动态增加,类似sprintf方法
     * @param string $keyName  一个cacher常量
     * @param mixed $args [optional],
     * @return string
     */
    public static function getKey($keyName, $args = null)
    {
        $funcArgs = func_get_args();
        unset($funcArgs[0]);
        return is_null($args) ? $keyName : vsprintf($keyName, $args);
    }
    
    /**
     * 获取缓存值,无缓存或获取失败返回false
     * @param string $key
     * @return mixed 无缓存时返回false
     */
    public static function get($key)
    {
        return static::getCacheProxy()->get($key);
    }
    
    /**
     * 设置缓存,失败时返回false
     * @param string $key
     * @param mixed $value
     * @param int $duration
     * @param Dependency $dependency
     * @return bool
     */
    public static function set($key, $value, $duration = null, $dependency = null)
    {
        return static::getCacheProxy()->set($key, $value, $duration, $dependency);
    }
    
    /**
     * 获取或设置
     * @param string $key
     * @param callable $callable 获取值的函数
     * @param int $duration
     * @param Dependency $dependency
     * @return bool
     */
    public static function getOrSet($key, $callable, $duration = null, $dependency = null)
    {
        return static::getCacheProxy()->getOrSet($key, $callable, $duration, $dependency);
    }

    /**
     * 判断缓存键是否存在,存在返回true
     * @param string $key
     * @return bool
     */
    public static function exists($key)
    {
        return static::getCacheProxy()->exists($key);
    }
    
    /**
     * 删除缓存
     * @param string $key
     * @return bool
     */
    public static function delete($key)
    {
        return static::getCacheProxy()->delete($key);
    }
    
    /**
     * 清空缓存,成功返回true
     * @return bool
     */
    public static function flush()
    {
        return static::getCacheProxy()->flush();
    }

    private static function getCacheProxy()
    {
        return \yii::$app->cache;
    }
}

<?php

namespace iUtils;

/**
 * 错误码,业务可继续该类自定义错误
 * -999 - 999为ErrorCode内部预留错误码,业务错误码为4位数字
 * 1位为系统级错误,2位应用异常,3位应用错误
 * @author ray
 */
class Error
{
    const SYSTEM_BUSY = '系统繁忙|1';
    
    const SYSTEM_ERROR = '系统错误|-1';
    
    const PARAM_INVALIDATE = '缺少或参数无效|400';
    
    const CONFIG_MISS = '配置丢失|500';
    
    const DATA_FORBIDDEN = '无数据权限|403';
    
    const OUT_LIMIT_RANGE = '超过限制范围|401';
    
    /**
     * 获取一个标准的数组错误返回
     * @param string $errConst ErrorCode类常量
     * @param string $customMsg 自定义错误信息
     * @return array ['errcode' => 错误码, 'errmsg' => 错误描述]
     */
    public static function format($errConst, $customMsg = '')
    {
        list($errMsg, $errCode) = explode('|', $errConst);
        return ['errcode' => (int)$errCode, 'errmsg' => $errMsg . ($customMsg ? "({$customMsg})" : "")];
    }
    
    /**
     * 获取json字符串返回
     * @param string $errConst ErrorCode类常量
     * @param string $customMsg 自定义错误信息
     * @return string {"errcode":错误码, "errmsg":错误描述}
     */
    public static function json($errConst, $customMsg = '')
    {
        $data = static::format($errConst, $customMsg);
        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }
    
    
}

<?php

namespace iUtils\helpers;

/**
 * 数组助手类
 *
 * @author ray
 */
class ArrayHelper
{
    /**
     * Indexes an array according to a specified key.
     * The input array should be multidimensional or an array of objects.
     *
     * The key can be a key name of the sub-array, a property name of object, or an anonymous
     * function which returns the key value given an array element.
     *
     * If a key value is null, the corresponding array element will be discarded and not put in the result.
     *
     * For example,
     *
     * ~~~
     * $array = [
     *     ['id' => '123', 'data' => 'abc'],
     *     ['id' => '345', 'data' => 'def'],
     * ];
     * $result = ArrayHelper::index($array, 'id');
     * // the result is:
     * // [
     * //     '123' => ['id' => '123', 'data' => 'abc'],
     * //     '345' => ['id' => '345', 'data' => 'def'],
     * // ]
     *
     * // using anonymous function
     * $result = ArrayHelper::index($array, function ($element) {
     *     return $element['id'];
     * });
     * ~~~
     *
     * @param array $array the array that needs to be indexed
     * @param string|\Closure $key the column name or anonymous function whose result will be used to index the array
     * @return array the indexed array
     */
    public static function index($array, $key)
    {
        $result = [];
        foreach ($array as $element) {
            $value = static::getValue($element, $key);
            $result[$value] = $element;
        }

        return $result;
    }
    
    /**
     * Builds a map (key-value pairs) from a multidimensional array or an array of objects.
     * The `$from` and `$to` parameters specify the key names or property names to set up the map.
     * Optionally, one can further group the map according to a grouping field `$group`.
     *
     * For example,
     *
     * ~~~
     * $array = [
     *     ['id' => '123', 'name' => 'aaa', 'class' => 'x'],
     *     ['id' => '124', 'name' => 'bbb', 'class' => 'x'],
     *     ['id' => '345', 'name' => 'ccc', 'class' => 'y'],
     * ];
     *
     * $result = ArrayHelper::map($array, 'id', 'name');
     * // the result is:
     * // [
     * //     '123' => 'aaa',
     * //     '124' => 'bbb',
     * //     '345' => 'ccc',
     * // ]
     *
     * $result = ArrayHelper::map($array, 'id', 'name', 'class');
     * // the result is:
     * // [
     * //     'x' => [
     * //         '123' => 'aaa',
     * //         '124' => 'bbb',
     * //     ],
     * //     'y' => [
     * //         '345' => 'ccc',
     * //     ],
     * // ]
     * ~~~
     *
     * @param array $array
     * @param string|\Closure $from
     * @param string|\Closure $to
     * @param string|\Closure $group
     * @return array
     */
    public static function map($array, $from, $to, $group = null)
    {
        $result = [];
        foreach ($array as $element) {
            $key = static::getValue($element, $from);
            $value = static::getValue($element, $to);
            if ($group !== null) {
                $result[static::getValue($element, $group)][$key] = $value;
            } else {
                $result[$key] = $value;
            }
        }

        return $result;
    }
    
    /**
     * 分组
     *$array = [
     *     ['id' => '123', 'name' => 'aaa', 'class' => 'x'],
     *     ['id' => '123', 'name' => 'bbb', 'class' => 'x'],
     *     ['id' => '345', 'name' => 'ccc', 'class' => 'y'],
     * ];
     * group($array, 'class', ['id','name'])
     * $array = [
     *    'x' => [['id' => '123', 'name' => 'aaa'],['id' => '123', 'name' => 'bbb']]
     *    'y' => [['id' => '345', 'name' => 'ccc']]
     * ];
     * @param array $array
     * @param string $groupKey
     * @param array $selectKeys
     */
    public static function group($array, $groupKey, $selectKeys)
    {
        $result = [];
        foreach ($array as $element) {
            if (static::keyExists($element[$groupKey], $result)) {
               $result[$element[$groupKey]][] = static::get($element, $selectKeys);
            } else {
                $result[$element[$groupKey]] = [static::get($element, $selectKeys)];
            }
        }
        
        return $result;
    }
    
    /**
     * 获取指定key的数组,过滤到不需要的key
     * $array = [
     *     ['id' => '123', 'name' => 'aaa', 'class' => 'x'],
     *     ['id' => '124', 'name' => 'bbb', 'class' => 'x'],
     *     ['id' => '345', 'name' => 'ccc', 'class' => 'y'],
     * ];
     * get($array, ['id',name])
     * $array = [
     *     ['id' => '123', 'name' => 'aaa'],
     *     ['id' => '124', 'name' => 'bbb'],
     *     ['id' => '345', 'name' => 'ccc'],
     * ];
     * 
     * $array = ['id' => '123', 'name' => 'aaa', 'class' => 'x']
     * get($array, ['id',name])
     * $array = ['id' => '123', 'name' => 'aaa']
     * 
     * @param array $array
     * @param array $keys
     * @return array
     */
    public static function get($array, $keys)
    {
        $result = [];
        foreach ($array as $k => $element) {
            if (is_array($element)) {
                $item = [];
                foreach ($keys as $key) {
                    if (key_exists($key, $element)) {
                        $item[$key] = $element[$key];
                    }
                }

                if ($item) {
                    $result[] = $item;
                }
            } else {
                if (in_array($k, $keys)) {
                    $result[$k] = $element;
                }
            }
        }
        
        return $result;
    }

    /**
     * 转换为字典
     * $array = [
     *     ['id' => '123', 'name' => 'aaa', 'class' => 'x'],
     *     ['id' => '124', 'name' => 'bbb', 'class' => 'x'],
     *     ['id' => '345', 'name' => 'ccc', 'class' => 'y'],
     * ];
     *
     * $result = ArrayHelper::toDictionary($array, 'id', 'name');
     * // the result is:
     * // [
     * //     '123' => 'aaa',
     * //     '124' => 'bbb',
     * //     '345' => 'ccc',
     * // ]
     *
     * @param array $array
     * @param string $key 字典key
     * @param mixed $value 字典value
     * @return array
     */
    public static function toDictionary($array, $key, $value)
    {
        return static::map($array, $key, $value);
    }

    /**
     * 从数组中移除某个元素,未找到则返回指定的默认值
     * @param array $array the array to extract value from
     * @param string $key key name of the array element
     * @param mixed $default the default value to be returned if the specified key does not exist
     * @return mixed|null the value of the element if found, default value otherwise
     */
    public static function remove(&$array, $key, $default = null)
    {
        if (is_array($array) && (isset($array[$key]) || array_key_exists($key, $array))) {
            $value = $array[$key];
            unset($array[$key]);

            return $value;
        }

        return $default;
    }
    
    /**
     * 过滤掉空值
     * @param array $array
     */
    public static function filterEmpty(&$array)
    {
        $array = array_filter($array);
    }

    /**
     * 过滤出满足条件的数组元素
     * $array = [['id'=>1, 'name' => 'zhangs', 'age' => 19],['id'=>2, 'name' => 'lis', 'age' => 19],['id'=>1, 'name' => 'wangw', 'age' => 20]]
     * filter($array, ['age' => 19]) 返回 [['id'=>1, 'name' => 'zhangs', 'age' => 19],['id'=>2, 'name' => 'lis', 'age' => 19]]
     * filter($array, ['age' => 19, 'name' => 'zhangs'])  返回 [['id'=>1, 'name' => 'zhangs', 'age' => 19]]
     * @param array $array
     * @param array $condition
     * @return array
     */
    public static function filter($array, $condition)
    {
        foreach ($condition as $key => $value) {
            $array = array_filter($array, function($v) use($key, $value){
                return key_exists($key, $v) && $v[$key] === $value;
            });
        }
        
        return $array;
    }
    
    /**
     * 查找满足条件的单行,找不到返回空数组
     * @param array in array $array
     * @param array $condition
     * @return array
     */
    public static function findOne($array, $condition)
    {
        $items = static::filter($array, $condition);
        return $items ? array_values($items)[0] : [];
    }

    /**
     * 检查某个key是否存在,默认大小写敏感,存在返回true
     * @param string $key the key to check
     * @param array $array the array with keys to check
     * @param boolean $caseSensitive whether the key comparison should be case-sensitive
     * @return boolean whether the array contains the specified key
     */
    public static function keyExists($key, $array, $caseSensitive = true)
    {
        if ($caseSensitive) {
            return array_key_exists($key, $array);
        } else {
            foreach (array_keys($array) as $k) {
                if (strcasecmp($key, $k) === 0) {
                    return true;
                }
            }

            return false;
        }
    }

    /**
     * 获取指定key的值,未找到则返回默认值
     * key为`name`格式时,返回$array['name']或$array->name
     * key为`x.y.z`格式时,返回`$array['x']['y']['z']` or `$array->x->y->z`
     * key为['1.0', 'date']时,返回$array['1.0']['date']
     * @param array|object $array
     * @param string|array $key
     * @param mixed $default
     * @return mixed
     */
    public static function getValue($array, $key, $default = null)
    {
        if ($key instanceof \Closure) {
            return $key($array, $default);
        }

        if (is_array($key)) {
            $lastKey = array_pop($key);
            foreach ($key as $keyPart) {
                $array = static::getValue($array, $keyPart);
            }
            $key = $lastKey;
        }

        if (is_array($array) && array_key_exists($key, $array)) {
            return $array[$key];
        }

        if (($pos = strrpos($key, '.')) !== false) {
            $array = static::getValue($array, substr($key, 0, $pos), $default);
            $key = substr($key, $pos + 1);
        }

        if (is_object($array)) {
            return $array->$key;
        } elseif (is_array($array)) {
            return array_key_exists($key, $array) ? $array[$key] : $default;
        } else {
            return $default;
        }
    }
    
    /**
     * 从数组中获取指定key集合的值
     * $a = ['zhangs' => '张三', 'lis' => '李四', 'wangw' => '王五']
     * $result = getValues($a, ['zhangs', 'wangw'])
     * $result为['张三','王五']
     * @param array $array
     * @param array $keys
     * @return array
     */
    public static function getValues($array, $keys)
    {
        $values = [];
        foreach ($keys as $key) {
            if (key_exists($key, $array)) {
                $values[] = $array[$key];
            }
        }
        
        return $values;
    }

        /**
    * Converts array to object
    * @param  object  $obj
    * @return object
    */
    public static function toObject(array $arr, $obj)
    {
        foreach ($arr as $k => $v) {
            $obj->$k = $v;
        }
        return $obj;
    }
    
    /**
     * 判断值是否在指定数组中,如果第三个参数为false,由返回bool值,否则返回第一个匹配的key
     * @param mixed $value
     * @param array $array
     * @param bool $returnKey 是否返回对应的key
     * @return bool|key
     */
    public static function in($value, array $array, $returnKey = false)
    {
        $inArray = in_array($value, $array, true);
        if ($returnKey) {
            if ($inArray) {
                return array_search($value, $array, true);
            }
            
            return false;
        }
        
        return $inArray;
    }
    
    /**
     * Returns the first element in an array.
     *
     * @param  array $array
     * @return mixed
     */
    public static function first(array $array)
    {
        return reset($array);
    }
    
    /**
     * Returns the last element in an array.
     *
     * @param  array $array
     * @return mixed
     */
    public static function last(array $array)
    {
        return end($array);
    }
    
    /**
     * Returns the first key in an array.
     *
     * @param  array $array
     * @return int|string
     */
    public static function firstKey(array $array)
    {
        reset($array);
        return key($array);
    }
    
    /**
     * Returns the last key in an array.
     *
     * @param  array $array
     * @return int|string
     */
    public static function lastKey(array $array)
    {
        end($array);
        return key($array);
    }
    
    /**
     * 在数组中追回key
     * @param array $array 要追回的数组
     * @param string $key 要添加的key
     * @param callable $valueCallback
     */
    public static function appendKey(array &$array, $key, $valueCallback)
    {
        array_walk($array, function(&$a) use($key, $valueCallback) {
           $a[$key] = call_user_func($valueCallback, $a); 
        });
    }
}

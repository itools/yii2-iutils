<?php

namespace iUtils\helpers;

/**
 * string助手类
 *
 * @author ray
 */
class StringHelper
{
    /**
     * 36位uuid
     * @param int $versionNumber
     * @return mixed
     */
    public static function uuid($versionNumber = 1)
    {
        if (!in_array($versionNumber, [1, 4], false)){
            throw new \InvalidArgumentException('版本号有误');
        }

        $funcName = "uuid{$versionNumber}";
        return \Ramsey\Uuid\Uuid::$funcName()->toString();
    }
    
    /**
     * 自动转换字符集 支持数组转换
     * @param string $string 要转换的字符串
     * @param string $from 原字符集
     * @param string $to 新字符集
     * @return string
     */
    public static function autoCharset($string, $from = 'gbk', $to = 'utf-8')
    {
        $from = strtoupper($from) == 'UTF8' ? 'utf-8' : $from;
        $to = strtoupper($to) == 'UTF8' ? 'utf-8' : $to;
        if (strtoupper($from) === strtoupper($to) || empty($string) || (is_scalar($string) && !is_string($string))) {
            //如果编码相同或者非字符串标量则不转换
            return $string;
        }
        if (is_string($string)) {
            if (function_exists('mb_convert_encoding')) {
                return mb_convert_encoding($string, $to, $from);
            } elseif (function_exists('iconv')) {
                return iconv($from, $to, $string);
            } else {
                return $string;
            }
        } elseif (is_array($string)) {
            foreach ($string as $key => $val) {
                $_key = self::autoCharset($key, $from, $to);
                $string[$_key] = self::autoCharset($val, $from, $to);
                if ($key != $_key)
                    unset($string[$key]);
            }
            return $string;
        }
        else {
            return $string;
        }
    }

    /**
     * 转换为json字符串
     * @param mixed $value
     * @return string
     */
    public static function jsonEncode($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }

    /**
     * 判断字符串是否以某字符串开头
     * @param $str 原串
     * @param $needle 部份串
     * @return bool
     */
    public static function startWith($str, $needle)
    {
        return strpos($str, $needle) === 0;
    }


    /**
     * 判断字符串是否以某字符串结尾
     * @param $haystack 原串
     * @param $needle 部份串
     * @return bool
     */
    public static function endWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }
        
        return (substr($haystack, -$length) === $needle);
    }
    
    /**
     * 判断字符串中是否包括某部分
     * @param string $haystack 原串
     * @param string $needle 部份串
     * @return bool
     */
    public static function contains(string $haystack, string $needle)
    {
        return strpos($haystack, $needle) !== false;
    }

    /**
     * 截取字符串
     * @param string $str
     * @param int $start
     * @param int $length
     * @return type
     */
    public static function substring(string $str, int $start, int $length = null)
    {
        if (function_exists('mb_substr')) {
                return mb_substr($str, $start, $length, 'UTF-8'); // MB is much faster
        } elseif ($length === null) {
                $length = self::length($str);
        } elseif ($start < 0 && $length < 0) {
                $start += self::length($str); // unifies iconv_substr behavior with mb_substr
        }
        
        return iconv_substr($str, $start, $length, 'UTF-8');
    }
        
    /**
     * 从左边截取指定长度的部分
     * @param string $str
     * @param int $maxLen
     * @param string $append
     * @return string
     */
    public static function truncate(string $str, int $maxLen, string $append = "\u{2026}")
    {
        if (self::length($str) > $maxLen) {
                $maxLen -= self::length($append);
                if ($maxLen < 1) {
                        return $append;
                } elseif ($matches = self::match($str, '#^.{1,' . $maxLen . '}(?=[\s\x00-/:-@\[-`{-~])#us')) {
                        return $matches[0] . $append;
                } else {
                        return self::substring($str, 0, $maxLen) . $append;
                }
        }

        return $str;
    }
    
    /**
     * 删除字符串两边的空白符
     * @param string $str
     * @return type
     */
    public static function trim(string $str)
    {
        $charlist = " \t\n\r\0\x0B\u{A0}";
        $charlist = preg_quote($charlist, '#');
        return self::replace($str, '#^[' . $charlist . ']+|[' . $charlist . ']+\z#u', '');
    }
        
    /**
     * 移除字符串中的特殊符号
     * @param $strParam
     * @return mixed
     */
    static function replaceSpecialChar($strParam, $ignore = [])
    {
        $regex = "/\/|\~|\!|\@|\#|\\$|\%|\^|\&|\*|\"|\(|\)|\_|\+|\{|\}|\:|\<|\>|\?|\[|\]|\,|\.|\/|\;|\'|\`|\-|\=|\\\|\||\s{2,}| |\xc2\xa0/";
        if ($ignore) {
            foreach ($ignore as $char) {
                if ($char == '\\') {
                    $search = '\\\|';
                } else {
                    $search = '\\' . $char . '|';
                }

                $regex = str_replace($search, '', $regex);
            }
        }

        return preg_replace($regex, "", $strParam);
    }
    
    /**
     * 获取一个安全的随机字符
     * @param string $type alnum, alpha, hexdec, numeric, nozero, distinct
     * @param int $length
     * @return string
     */
    public static function random($type = 'alnum', $length=16)
    {
        switch ($type) {
            case 'alnum':
                $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'alpha':
                $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'hexdec':
                $pool = '0123456789abcdef';
                break;
            case 'numeric':
                $pool = '0123456789';
                break;
            case 'nozero':
                $pool = '123456789';
                break;
            case 'distinct':
                $pool = '2345679ACDEFHJKLMNPRSTUVWXYZ';
                break;
            default:
                $pool = (string)$type;
                break;
        }


        $crypto_rand_secure = function ($min, $max) {
            $range = $max - $min;
            if ($range < 0) return $min; // not so random...
            $log = log($range, 2);
            $bytes = (int)($log / 8) + 1; // length in bytes
            $bits = (int)$log + 1; // length in bits
            $filter = (int)(1 << $bits) - 1; // set all lower bits to 1
            do {
                $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
                $rnd = $rnd & $filter; // discard irrelevant bits
            } while ($rnd >= $range);
            return $min + $rnd;
        };

        $token = "";
        $max = strlen($pool);
        for ($i = 0; $i < $length; $i++) {
            $token .= $pool[$crypto_rand_secure(0, $max)];
        }
        return $token;
    }
    
    /**
     * 判断字符串是否全是中文
     * @param string $str
     * @return bool
     */
    public static function isChinese($str)
    {
       $len = preg_match('/^[\x{4e00}-\x{9fa5}]+$/u', $str);
       return $len ? true : false;
    }
    
    /**
     * 判断是否有效的json字符串
     * @param string $str
     * @return bool
     */
    public static function isJson($str)
    {
        return !is_null(json_decode($str));
    }

    /**
     * 将url中中文部分进行urlencode编码
     * @param $url
     * @return mixed
     */
    public static function encodeChineseUrl($url)
    {
        $pregstr = "/[\x{4e00}-\x{9fa5}]+/u";//中文正则
        if (preg_match_all($pregstr, $url, $matchArray)) {
            foreach ($matchArray[0] as $key => $val) {
                $url = str_replace($val, urlencode($val), $url);//将转译替换中文
            }
            if (strpos($url, ' ')) {//若存在空格
                $url = str_replace(' ', '%20', $url);
            }
        }
        return $url;
    }
}

<?php

namespace iUtils\helpers;

/**
 * 日期时间助手类
 *
 * @author ray
 */
class DateTimeHelper
{
    const UNIT_YEAR = 'y';
    
    const UNIT_MONTH = 'm';
    
    const UNIT_DAY = 'd';
    
    const UNIT_HOUR = 'h';
    
    const UNIT_MINITUS = 'm';
    
    const UNIT_SECOND = 's';
    
    const FORMAT_DATE = 'Y-m-d';
    
    const FORMAT_DATETIME = 'Y-m-d H:i:s';
    
    /**
     * 当前系统时间，精确到秒
     * @param string $format 格式,默认2009-01-04 09:20:04
     * @param string $timezone 时区
     * @return bool|string
     */
    public static function now($format = 'Y-m-d H:i:s', $timezone = 'PRC')
    {
        date_default_timezone_set($timezone);
        return date($format, time());
    }

    /**
     * 添加年月日,返回日期字符串 如:2018-09-01
     * @param string $date
     * @param int $int
     * @param string $unit y年 m月 d日
     * @return string
     */
    public static function addDate($date, $int, $unit = 'd')
    {
        $value = array('y' => 0, 'm' => 0, 'd' => 0);
        $dateArr = explode("-", $date);
        if (array_key_exists($unit, $value)) {
            $value[$unit] = $int;
        } else {
            return false;
        }
        return date(static::FORMAT_DATE, mktime(0, 0, 0, $dateArr[1] + $value['m'], $dateArr[2] + $value['d'], $dateArr[0] + $value['y']));
    }

    /**
     * 添加年月日时分秒,返回日期时间字符串 如:2018-09-01 02:10:01
     * @param string $date
     * @param int $int
     * @param string $unit
     * @return string
     */
    public static function addDateTime($date, $int, $unit = 'd')
    {
        $value = array('y' => 0, 'm' => 0, 'd' => 0, 'h' => 0, 'i' => 0, 's' => 0);
        $dateArr = preg_split("/-|\s|:/", $date);
        if (array_key_exists($unit, $value)) {
            $value[$unit] = $int;
        } else {
            return false;
        }
        return date(static::FORMAT_DATETIME, mktime($dateArr[3] + $value['h'], $dateArr[4] + $value['i'], $dateArr[5] + $value['s'], $dateArr[1] + $value['m'], $dateArr[2] + $value['d'], $dateArr[0] + $value['y']));
    }
    
    /**
     * 时间比较函数，返回两个日期相差几秒、几分钟、几小时或几天
     * @param string $begin
     * @param string $end
     * @param string $unit
     * @return int
     */
    public static function dateDiff($begin, $end, $unit = 'd')
    {
        $diff = strtotime($end) - strtotime($begin);
        switch ($unit) {
            case "y": $retval = bcdiv($diff, (60 * 60 * 24 * 365));
                break;
            case "m": $retval = bcdiv($diff, (60 * 60 * 24 * 30));
                break;
            case "w": $retval = bcdiv($diff, (60 * 60 * 24 * 7));
                break;
            case "d": $retval = bcdiv($diff, (60 * 60 * 24));
                break;
            case "h": $retval = bcdiv($diff, (60 * 60));
                break;
            case "i": $retval = bcdiv($diff, 60);
                break;
            case "s": $retval = $diff;
                break;
        }
        return $retval;
    }
    
    /**
     * 获取当前时间戳,支持秒级、毫秒、微秒级
     * @param string $type 时间戳类型,默认秒
     * @param string $timezone 时区
     * @return int|float
     * @throws \yii\base\NotSupportedException
     */
    public static function getTimestamp($type = 'sec', $timezone = 'PRC')
    {
        if (!in_array($type, ['sec', 'mil', 'micro'])) {
            throw new \yii\base\NotSupportedException("不支持的时间戳类型{$type}");
        }
        
        date_default_timezone_set($timezone);
        // 秒时间戳
        if ($type == 'sec') {
            return time();
        }
        
        list($msec, $sec) = explode(' ', microtime());
        // 毫秒时间戳
        if ($type == 'mil') {
            return (float)sprintf('%.0f', (floatval($sec) + floatval($msec)) * 1000);
        }
        
        // 微秒时间戳
        if ($type == 'micro') {
            return (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000 * 1000);
        } 
    }
    
    /**
     * 获取当前时间指定部分,支持毫秒和微秒
     * @param string $part
     * @param string $timezone
     * @return string
     */
    public static function getPartOfNow($part, $timezone = 'PRC')
    {
        date_default_timezone_set($timezone);
        // 毫秒
        if ($part == 'v') {
            list($msec, $sec) = explode(' ', microtime());
            return sprintf('%.0f', floatval($msec) * 1000);
        }
        
        // 微秒
        if ($part == 'u') {
            list($msec, $sec) = explode(' ', microtime());
            return sprintf('%.0f', floatval($msec) * 1000 * 1000);
        }
        
        return date($part, time());
    }
}

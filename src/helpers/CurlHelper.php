<?php

namespace iUtils\helpers;

use linslin\yii2\curl\Curl;

/**
 * Curl 帮助类
 *
 * @author YashonLvan
 */
class CurlHelper
{
    public static $client;

    public static $timeout = 30;

    public static function getClient()
    {
        if (empty(self::$client)) {
            self::$client = new Curl();
        } else {
            self::$client->reset();
        }

        return self::$client;
    }

    /**
     * @param $url
     * @param array $params
     * @param bool $throwException
     * @param array $headers
     * @param null $timeout
     * @return bool|mixed
     * @throws \Exception
     */
    public static function get($url, $params = [], $throwException = false, $headers = [], $timeout = null)
    {
        $curl = self::getClient();
        $response = $curl->setGetParams($params)
            ->setHeaders($headers)
            ->setOption(CURLOPT_TIMEOUT, $timeout ?: self::$timeout)
            ->get($url);

        if ($curl->errorCode !== null) {
            if ($throwException) {
                throw new \RuntimeException($curl->errorText);
            }
            return false;
        }

        $resultArr = json_decode($response, true);

        if (!is_array($resultArr)) {
            return $curl;
        }

        return $resultArr;
    }

    /**
     * post请求
     * @param $url
     * @param array $params
     * @param bool $throwException
     * @param array $headers
     * @param null $timeout
     * @return bool|mixed
     * @throws \Exception
     */
    public static function post($url, $params = [], $throwException = false, $headers = [], $timeout = null)
    {
        $curl = self::getClient();
        $response = $curl->setPostParams($params)
            ->setHeaders($headers)
            ->setOption(CURLOPT_TIMEOUT, $timeout ?: self::$timeout)
            ->post($url);
        if ($curl->errorCode !== null) {
            if ($throwException) {
                throw new \RuntimeException($curl->errorText);
            }
            return false;
        }

        $resultArr = json_decode($response, true);

        if (!is_array($resultArr)) {
            return $curl;
        }
        return $resultArr;
    }

    /**
     * raw请求
     * @param $url
     * @param array $params
     * @param bool $throwException
     * @param null $timeout
     * @param array $headers
     * @return bool|mixed
     * @throws \Exception
     */
    public static function raw($url, $params = [], $throwException = false, $headers = [], $timeout = null)
    {
        $curl = self::getClient();
        $response = $curl->setHeader('Content-Type', 'application/json')
            ->setHeaders($headers)
            ->setRawPostData(json_encode($params, JSON_UNESCAPED_UNICODE))
            ->setOption(CURLOPT_TIMEOUT, $timeout ?: self::$timeout)
            ->post($url);
        if ($curl->errorCode !== null) {
            if ($throwException) {
                throw new \RuntimeException($curl->errorText);
            }
            return false;
        }

        $resultArr = json_decode($response, true);

        if (!is_array($resultArr)) {
            return $curl;
        }
        return $resultArr;
    }

}
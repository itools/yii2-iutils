<?php

namespace iUtils\yii2;

use yii\base\UserException;
use iUtils\exceptions\BusinessException;
use yii\web\Response;
use yii\web\HttpException;
/**
 * 自定义web处理类,以转换处理响应格式与controller一致
 * 业务异常可以直接抛出BusinessException
 * 使用:在配置文件中component节增加
 * 'errorHandler' => [
 *           'errorAction' => 'site/error',
 *           'class' => 'iUtils\yii2\WebErrorHandler'
 *       ],
 *
 * @author ray
 */
class WebErrorHandler extends \yii\web\ErrorHandler
{
     /**
     * Renders the exception.
     * @param \Exception $exception the exception to be rendered.
     */
    protected function renderException($exception)
    {
        if (\Yii::$app->has('response')) {
            $response = \Yii::$app->getResponse();
            // reset parameters of response to avoid interference with partially created response data
            // in case the error occurred while sending the response.
            $response->isSent = false;
            $response->stream = null;
            $response->data = null;
            $response->content = null;
        } else {
            $response = new Response();
        }

        $useErrorView = $response->format === Response::FORMAT_HTML && (!YII_DEBUG || $exception instanceof UserException);

        if ($useErrorView && $this->errorAction !== null) {
            $result = \Yii::$app->runAction($this->errorAction);
            if ($result instanceof Response) {
                $response = $result;
            } else {
                $response->data = $result;
            }
        } elseif ($response->format === Response::FORMAT_HTML) {
            if (YII_ENV_TEST || isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest') {
                // AJAX request
                $response->data = '<pre>' . $this->htmlEncode($this->convertExceptionToString($exception)) . '</pre>';
            } else {
                // if there is an error during error rendering it's useful to
                // display PHP error in debug mode instead of a blank screen
                if (YII_DEBUG) {
                    ini_set('display_errors', 1);
                }
                $file = $useErrorView ? $this->errorView : $this->exceptionView;
                $response->data = $this->renderFile($file, [
                    'exception' => $exception,
                ]);
            }
        } elseif ($response->format === Response::FORMAT_RAW) {
            $response->data = $exception;
        } else {
            $response->data = $this->convertExceptionToArray($exception);
        }

        if ($exception instanceof HttpException || is_subclass_of($exception, 'yii\web\HttpException')) {
            $response->setStatusCode($exception->statusCode);
        } else {
            // 非http错误,正常返回用于前端判断
            $response->setStatusCode(200);
        }

        $response->send();
    }
    
    protected function convertExceptionToArray($exception)
    {
        if (!YII_DEBUG && !$exception instanceof BusinessException && !$exception instanceof HttpException) {
            $exception = new HttpException(500, '系统异常,请稍后再试');
        }
        
        // 统一json返回格式
        $array = [
            'errcode' => $exception->getCode() ?: -1,
            'errmsg' => $exception->getMessage()
        ];
        
        if ($exception instanceof HttpException) {
            $array['status'] = $exception->statusCode;
        }
        if (YII_DEBUG) {
            $array['type'] = get_class($exception);
            if (!$exception instanceof BusinessException) {
                $array['file'] = $exception->getFile();
                $array['line'] = $exception->getLine();
                $array['stack-trace'] = explode("\n", $exception->getTraceAsString());
                if ($exception instanceof \yii\db\Exception) {
                    $array['error-info'] = $exception->errorInfo;
                }
            }
        }
        if (($prev = $exception->getPrevious()) !== null) {
            $array['previous'] = $this->convertExceptionToArray($prev);
        }

        return $array;
    }
}

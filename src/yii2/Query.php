<?php

namespace iUtils\yii2;

use Yii;
use yii\db\Command;

/**
 * Created by PhpStorm.
 * User: zhangqing
 * Date: 2020-01-20
 * Time: 11:17
 */
class Query extends \yii\db\Query
{
    /**
     * @var Command
     */
    protected $command;

    /**
     * Creates a DB command that can be used to execute this query.
     * @param \yii\db\Connection $db the database connection used to generate the SQL statement.
     * If this parameter is not given, the `db` application component will be used.
     * @return \yii\db\Command the created DB command instance.
     */
    public function createCommand($db = null)
    {
        if ($db === null) {
            $db = Yii::$app->getDb();
        }
        list($sql, $params) = $db->getQueryBuilder()->build($this);

        $command = $db->createCommand($sql, $params);
        $this->setCommandCache($command);
        $this->command = $command;

        return $this->command;
    }

    /**
     * Get the query's DB command that can be used to execute this query.
     * @return \yii\db\Command the created DB command instance.
     * @return Command
     */
    public function getCommand()
    {
        return $this->command;
    }
}

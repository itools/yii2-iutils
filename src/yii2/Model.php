<?php

namespace iUtils\yii2;

use iUtils\helpers\ArrayHelper;
use iUtils\exceptions\BusinessException;

/**
 * 数据访问层基类,每个表应继续该类并重写tableName静态方法,
 * 要实现事务可通过setDb方法实现
 *
 * @author ray
 */
class Model extends \yii\base\BaseObject
{
    /**
     * 租户代码,用于支持saas模式
     * @var string
     */
    protected $dbCode;

    protected $dbConnection;

    /**
     * 表别名
     * @var string
     */
    protected $alias;

    /**
     * 软删除字段标识（可继承该类后改写该字段）
     * @var string
     */
    protected $softDeleteField = 'is_deleted';

    /**
     * 表是否开启软删除（默认开启，查询的时候默认会加上{softDeleteField} = 0，如果不需要添加该条件该字段设为false，参照setSoftDelete方法）
     * @var boolean
     */
    protected $enableSoftDelete = true;

    /**
     * 是否去重
     * @var boolean
     */
    protected $distinct = false;

    /**
     * 连表查询数组
     * @var array
     *
     * ```php
     * [
     *     ['innerJoin', 'user', 'user.id = author_id'],
     *     ['leftJoin', 'team', 'team.id = team_id'],
     * ]
     * ```
     */
    protected $join;

    /**
     * query对象
     * @var Query
     */
    protected $query;

    /**
     * command对象
     * @var \yii\db\Command
     */
    protected $command;

    public function __construct($dbCode = '', $config = [])
    {
        $this->dbCode = $dbCode;
        parent::__construct($config);
    }

    /**
     * 获取表名,默认返回类名,子类重写此静态方法
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public static function tableName()
    {
        $reflector = new \ReflectionClass(self::class);
        if (PHP_VERSION_ID >= 70000 && $reflector->isAnonymous()) {
            throw new \yii\base\InvalidConfigException('The "tableName()" method should be explicitly defined for anonymous models');
        }

        return $reflector->getShortName();
    }

    /**
     * 设置数据库连接对象
     * @param \yii\db\connection $db
     * @return iModel
     */
    public function setDb($db)
    {
        $this->dbConnection = $db;
        return $this;
    }

    /**
     * 获取数据库连接对象
     * @return \yii\db\connection
     */
    public function getDb()
    {
        if ($this->dbConnection) {
            return $this->dbConnection;
        }

        return \yii::$app->db;
    }

    /**
     * 设置是否开启软删除
     * @param $enableSoftDelete
     */
    public function setSoftDelete($enableSoftDelete)
    {
        $this->enableSoftDelete = $enableSoftDelete;
    }

    /**
     * 获取是否开启软删除
     * @return boolean
     */
    public function getSoftDelete()
    {
        return $this->enableSoftDelete;
    }

    /**
     * 设置表别名
     * @param $name
     * @return Model
     */
    public function alias($name)
    {
        $this->alias = $name;
        return $this;
    }

    /**
     * 是否去重
     * @param bool $value
     * @return Model
     */
    public function distinct($value = true)
    {
        $this->distinct = $value;
        return $this;
    }

    /**
     * 添加连表查询
     * @param $type
     * @param $table
     * @param $alias
     * @param $on
     * @param $params
     * @throws
     */
    protected function addJoin($type, $table, $alias, $on = '', $params = [])
    {
        if (is_object($table)) {
            if (!$table instanceof Model) {
                throw new BusinessException('table类型不正确');
            }

            // 拼接逻辑删除语句
            $alias = $alias ? $alias : ($table->alias ? $table->alias : $table::tableName());
            if ($table->enableSoftDelete) {
                $on .= " and {$alias}.{$this->softDeleteField} = 0";
            }
            $tableName = $table::tableName();
        } else {
            $tableName = $table;
        }

        $this->join[] = [
            'type' => $type,
            'table' => "{$tableName} as {$alias}",
            'on' => $on,
            'params' => $params
        ];
    }

    /**
     * inner join连表
     * @param $table
     * @param $alias
     * @param $on
     * @param $params
     * @return Model
     * @throws
     */
    public function innerJoin($table, $alias, $on = '', $params = [])
    {
        $this->addJoin(__FUNCTION__, $table, $alias, $on, $params);
        return $this;
    }

    /**
     * left join连表
     * @param $table
     * @param $alias
     * @param $on
     * @param $params
     * @return Model
     * @throws
     */
    public function leftJoin($table, $alias, $on = '', $params = [])
    {
        $this->addJoin(__FUNCTION__, $table, $alias, $on, $params);
        return $this;
    }

    /**
     * right join连表
     * @param $table
     * @param $alias
     * @param $on
     * @param $params
     * @return Model
     * @throws
     */
    public function rightJoin($table, $alias, $on = '', $params = [])
    {
        $this->addJoin(__FUNCTION__, $table, $alias, $on, $params);
        return $this;
    }

    /**
     * 查询获取单条数据,无数据时返回空数组
     * @param array $conditions
     * // ...WHERE (`status` = 10) AND (`type` IS NULL) AND (`id` IN (4, 8, 15))
     *   $query->where([
     *       'status' => 10,
     *       'type' => null,
     *       'id' => [4, 8, 15],
     *   ]);
     * --like
     *   Where(['like', 'title','yii']);
     *  ... WHERE  (`title` LIKE '%yii%')
     * -- and
     *  Where(['and', 'id=1', 'parent_id=1']);
     *  ... WHERE id=1 AND parent_id=1
     * -- and or
     *  Where(['and', 'type=1', ['or', 'id=1', 'parent_id=1']]);
     *  ... WHERE type=1 AND (id=1 OR parent_id=1);
     * -- or
     * ->Where(['or like','name',['zhangs','lis']]);
     *   WHERE `name` LIKE '%zhangs%' OR `name` LIKE '%lis%';
     * -- and or
     *   Where(['or',['like','name','zhang'],['like','title','hello']]);//操作符格式的嵌套
     *  ... WHERE (`status`=1) AND ((`name` LIKE '%zhangs%') OR (`title` LIKE '%hello%'))
     * @param string|array $selects
     * @param $orderBy
     * @return array
     */
    public function queryOne($conditions, $selects, $orderBy = '')
    {
        $this->validateSelects($selects);
        $this->query = $this->getQueryWithWhere($conditions);
        if ($orderBy) {
            $this->query = $this->query->orderBy($orderBy);
        }

        $row = $this->query->select($selects)
            ->limit(1)
            ->one($this->getDb());

        return $row === false ? [] : $row;
    }

    /**
     * 查询获取满足条件的所有数据
     * @param array $conditions
     * @param string|array $selects
     * @param string|array $orderBy
     * @return array
     */
    public function queryAll($conditions, $selects, $orderBy = '')
    {
        $this->validateSelects($selects);
        $this->query = $this->getQueryWithWhere($conditions);
        if ($orderBy) {
            $this->query = $this->query->orderBy($orderBy);
        }

        return $this->query->select($selects)
            ->all($this->getDb()) ;
    }

    /**
     * 查询满足条件某个指定的值
     * @param array $conditions
     * @param string $colName
     * @param mixed $defaultValue
     * @return mixed
     */
    public function scalar($conditions, $colName, $defaultValue = false,  $orderBy = '')
    {
        $this->query = $this->getQueryWithWhere($conditions);
        if ($orderBy) {
            $this->query = $this->query->orderBy($orderBy);
        }

        $value = $this->query->select($colName)
            ->scalar($this->getDb());

        return $value === false ? $defaultValue : $value;
    }

    /**
     * 判断指定条件的记录是否存在
     * @param array $conditions
     * @return bool
     */
    public function exists($conditions)
    {
        $this->query = $this->getQueryWithWhere($conditions);
        return $this->query->exists($this->getDb());
    }

    /**
     * 查询满足条件某个列的值
     * @param array $conditions
     * @param string $colName
     * @param string $orderBy
     * @return mixed
     */
    public function column($conditions, $colName, $orderBy = '')
    {
        $this->query = $this->getQueryWithWhere($conditions);
        if ($orderBy) {
            $this->query = $this->query->orderBy($orderBy);
        }

        return $this->query->select($colName)
            ->column($this->getDb());
    }

    /**
     * 查询满足条件的特定记录
     * @param array $conditions
     * @param string $colName
     * @param $limit
     * @param string $orderBy
     * @return mixed
     */
    public function limit($conditions, $colName, $limit = 1, $orderBy = '')
    {
        $this->query = $this->getQueryWithWhere($conditions);
        if ($orderBy) {
            $this->query = $this->query->orderBy($orderBy);
        }

        return $this->query->select($colName)->limit($limit)->all($this->getDb());
    }

    /**
     * 查询满足条件的记录数
     * @param array $conditions
     * @param string $colName 若指定字段,值为null的不会计入
     * @return int
     */
    public function count($conditions, $colName = '*')
    {
        $this->query = $this->getQueryWithWhere($conditions);
        $cnt = $this->query->count($colName, $this->getDb());

        return $cnt;
    }

    /**
     * 统计满足条件的和
     * @param array $conditions
     * @param string $colName
     * @return number
     */
    public function sum($conditions, $colName)
    {
        $this->query = $this->getQueryWithWhere($conditions);
        $sum = $this->query->sum($colName, $this->getDb());

        return $sum;
    }

    /**
     * 分组统计满足条件的记录
     * @param array $conditions
     * @param string|array $groupCols
     * @param string|array $selects
     * @return array
     */
    public function group($conditions, $groupCols, $selects, $orderBy = '')
    {
        $this->validateSelects($selects);
        $this->query = $this->getQueryWithWhere($conditions);
        if ($orderBy) {
            $this->query = $this->query->orderBy($orderBy);
        }

        return $this->query->groupBy($groupCols)
            ->select($selects)
            ->all($this->getDb());
    }

    /**
     * 分页查询, paging为['page_index' => 1(从1开始), 'page_size' => 20]数组
     * @param array $conditions
     * @param array $paging
     * @param string|array $selects
     * @param string|array $orderBy
     * @return array
     */
    public function page($conditions, $paging, $selects, $orderBy = '')
    {
        $this->validateSelects($selects);
        if (!key_exists('page_index', $paging) || !$paging['page_index']) {
            $paging['page_index'] = 1;
        }
        
        if (!key_exists('page_size', $paging) || !$paging['page_size']) {
            $paging['page_size'] = 20;
        }

        $pageIndex = intval($paging['page_index']);
        $pageSize = intval($paging['page_size']);
        $pageSize = ($pageSize < 1) ? 1 : $pageSize;
        $pageSize = ($pageSize > 100) ? 100 : $pageSize;
        // where
        $this->query = $this->getQueryWithWhere($conditions);
        // order
        if ($orderBy) {
            $this->query = $this->query->orderBy($orderBy);
        }
        // count
        $this->query = $this->query->select($selects);
        $count = $this->query->count('*', $this->getDb());
        if ($count == 0) {
            return [
                'record_count' => 0,
                'page_size' => $pageSize < 1 ? 20 : $pageSize,
                'page_index' => $pageIndex < 1 ? 1 : $pageIndex,
                'data' => []
            ];
        }

        // page data
        $data = $this->query->offset(($pageIndex - 1)*$pageSize)->limit($pageSize)->all($this->getDb());
        return [
            'record_count' => intval($count),
            'page_size' => intval($pageSize),
            'page_index' => intval($pageIndex),
            'data' => $data
        ];
    }

    protected function validateSelects($selects)
    {
        if (empty($selects) || $selects == '*') {
            throw new \iUtils\exceptions\SqlException('不允许select *,必须指定字段名');
        }
    }

    /**
     * 自动识别构建where
     * @param array $conditions
     * @return object
     */
    protected function getQueryWithWhere($conditions)
    {
        $query = $this->createQuery();
        if (!$conditions) {
            return $this->appendSoftDeleteToQuery($query);
        }

        // key是index, value是数组, 则认为是多维条件 and and or等等
        $isMulti = is_int(ArrayHelper::firstKey($conditions)) && is_array(ArrayHelper::first($conditions));
        if ($isMulti) {
            $query = $query->where(array_shift($conditions));
            foreach ($conditions as $c) {
                $query = $query->andWhere($c);
            }
        } else {
            $query->where($conditions);
        }

        return $this->appendSoftDeleteToQuery($query);
    }

    /**
     * query对象追加软删除条件
     * @param \yii\db\Query
     * @return \yii\db\Query
     * @throws
     */
    protected function appendSoftDeleteToQuery($query)
    {
        if ($this->enableSoftDelete) {
            $tableName = !empty($this->alias) ? $this->alias : static::tableName();
            $softDeleteCondition = "{$tableName}.{$this->softDeleteField} = 0";
            if (!empty($query->where)) {
                $query->andWhere($softDeleteCondition);
            } else {
                $query->where($softDeleteCondition);
            }
        }

        return $query;
    }

    /**
     * 创建查询对象
     * @return object
     * @throws
     */
    protected function createQuery()
    {
        $this->command = null;
        $query = new Query();
        $tableName = !empty($this->alias) ? (static::tableName() . " as {$this->alias}") : static::tableName();
        $query->from($tableName);
        if (!empty($this->join)) {
            // 连表查询
            foreach ($this->join as $item) {
                $query->{$item['type']}($item['table'], $item['on'], $item['params']);
            }

            $this->join = [];
        }

        if ($this->distinct) {
            $query->distinct();
            $this->distinct = false;
        }

        return $query;
    }

    /**
     * 创建Command对象
     * @return object
     * @throws
     */
    public function createCommand()
    {
        $this->query = null;
        $this->command = $this->getDb()->createCommand();
        return $this->command;
    }

    /**
     * 单表更新,返回更新的行数
     * @param array $columns
     * @param string|array $condition
     * @return int
     */
    public function update($columns, $condition)
    {
        return $this->createCommand()->update(static::tableName(), $columns, $condition)->execute();
    }

    /**
     * 插入数据, 返回插入的行数,支持duplicateKey时更新
     * @param array $insertCols 插入字段
     * @param array $updateCols duplicateKey更新字段
     * @param string|array $duplicateKeys or关系
     * @return int
     */
    public function insert($insertCols, $updateCols = [], $duplicateKeys = '')
    {
        // 存在更新
        if ($duplicateKeys && $updateCols) {
            $keys = is_array($duplicateKeys) ?: explode(',', $duplicateKeys);
            $duplicate = ['or'];
            foreach ($keys as $key) {
                if (!key_exists($key, $insertCols)) {
                    throw new \iUtils\exceptions\SqlException("参数duplicateKeys中的:{$key} 不存在insertCols中");
                }

                $duplicate[] = [$key => $insertCols[$key]];
            }

            if ($this->exists($duplicate)) {
                return $this->update($updateCols, $duplicate);
            }
        }

        return $this->createCommand()->insert(static::tableName(), $insertCols)->execute();
    }

    /**
     * 批量插入数据,返回插入的行数,自动识别同列和不同列两种
     * @param array $rows [['id' => 1,'age' => 10], ['id' => 2,'age' => 12]] 或 [['id' => 1,'age' => 10], ['id' => 2,'age' => 12, 'name' => 'zhangs']]
     * @return int
     */
    public function batchInsert($rows)
    {
        $firstRow = ArrayHelper::first($rows);
        $columns = array_keys($firstRow);
        $insertRows = [];
        foreach ($rows as $row) {
            $insertRows[] = ArrayHelper::getValues($row, $columns);
        }

        return $this->createCommand()->batchInsert(static::tableName(), $columns, $insertRows)->execute();
    }

    /**
     * 单表删除(物理删除)
     * @param string|array $condition
     * @return int
     * @throws \Exception
     */
    public function delete($condition)
    {
        if (!$condition) {
            throw new \Exception('不允许执行不带条件的删除语句');
        }

        if ($this->enableSoftDelete) {
            // 逻辑删除
            return $this->update(["{$this->softDeleteField}" => 1], $condition);
        } else {
            // 物理删除
            return $this->createCommand()->delete(static::tableName(), $condition)->execute();
        }
    }

    /**
     * 获取执行的原始sql
     * @return string
     * @throws
     */
    public function getRawSql()
    {
        if (!empty($this->query) && !empty($this->query->getCommand())) {
            return $this->query->getCommand()->getRawSql();
        }

        if (!empty($this->command)) {
            return $this->command->getRawSql();
        }

        return '';
    }
}

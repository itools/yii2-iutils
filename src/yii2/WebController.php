<?php

namespace iUtils\yii2;

use iUtils\Error;
use iUtils\validate\Validator;
use yii\filters\ContentNegotiator;
use yii\web\Response;

/**
 * web controller基类,业务应继承该基类,并加入自定义身份验证和权限控制
 * 该基类统一json返回格式
 *
 * @property-read \yii\web\Request $request 请求
 * @property-read \yii\web\Response $response 响应
 *
 * @author ray
 */
class WebController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;

    protected $validator;

    public function __construct($id, $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->validator = Validator::newInstance();
    }

    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ]
        ];
    }

    /**
     * 请求
     * @return \yii\web\Request
     */
    public function getRequest()
    {
        return \yii::$app->request;
    }

    /**
     * 响应
     * @return \yii\web\Response
     */
    public function getResponse()
    {
        return \yii::$app->response;
    }

    /**
     * 获取url查询参数
     * @param string $name
     * @param mixed $defaultValue
     * @return mixed|array
     */
    public function get($name = null, $defaultValue = '')
    {
        return \yii::$app->request->get($name, $defaultValue);
    }

    /**
     * 获取post参数
     * @param string $name
     * @param mixed $defaultValue
     * @return mixed|array
     */
    public function post($name = null, $defaultValue = '')
    {
        return \yii::$app->request->post($name, $defaultValue);
    }

    /**
     * 获取raw参数
     * @param string $name
     * @param string $defaultValue
     * @return mixed|array
     */
    public function raw($name = null, $defaultValue = '')
    {
        $rawBody = \Yii::$app->request->rawBody;
        $data = json_decode($rawBody, true);
        if (is_null($name)) {
            return $data;
        }

        return key_exists($name, $data) ? $data[$name] : $defaultValue;
    }

    /**
     * 获取表单上传的文件(单个)
     * @param string $name 表单字段名
     * @return \yii\web\UploadedFile
     */
    public function file($name)
    {
        return \yii\web\UploadedFile::getInstanceByName($name);
    }

    /**
     * 获取获取表单上传的文件(批量)
     * @param string $name 表单字段名
     * @return array \yii\web\UploadedFile数组
     */
    public function files($name)
    {
        return \yii\web\UploadedFile::getInstancesByName($name);
    }

    /**
     * 分页输入
     * @param array $data 分页数据
     * @param int $recordCount 总记录数
     * @param int $pageSize 每页记录数
     * @param int $pageIndex 页码
     * @param array $additional 额外的数据,会合并到data上
     * @return array
     */
    public function page($data, $recordCount, $pageSize, $pageIndex, $additional = [])
    {
        $data = [
            'record_count' => intval($recordCount),
            'page_size' => intval($pageSize),
            'page_index' => intval($pageIndex),
            'data' => $data
        ];

        if ($additional) {
            $data = array_merge($data, $additional);
        }

        $response = ['errcode' => 0, 'errmsg' => 'ok', 'data' => $data];
        $this->response($response);
    }

    /**
     * 响应输出
     * @param array $data
     * @param int $errcode
     * @param string $errmsg
     * @return array
     */
    private function response($response)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        \Yii::$app->response->data = $response;
        \Yii::$app->end();
    }

    /**
     * 成功时输出,不需要加return
     * @param array $data
     * @return array
     */
    public function success($data = [])
    {
        $response = ['errcode' => 0, 'errmsg' => 'ok', 'data' => $data];
        $this->response($response);
    }

    /**
     * 错误时输出,不需要加return
     * @param string $errConst 一个Error常量
     * @param string $customErrMsg 自定义错误信息
     * @param \Exception $exception 异常对象
     * @param array $data 返回数据
     * @return array
     */
    public function error($errConst, $customErrMsg = '', $data = [], $exception = null)
    {
        list($errMsg, $errCode) = explode('|', $errConst);
        $response = [
            'errcode' => intval($errCode),
            'errmsg' => $errMsg . ($customErrMsg ? "({$customErrMsg})" : ''),
            'data' => $data
        ];

        if (YII_DEBUG && !is_null($exception)) {
            $response['exception'] = $exception;
        }

        $this->response($response);
    }

    /**
     * 参数校验
     * @param array $data 数据
     * @param array $rules 验证规则
     * @param Bool $isEnd 验证不通过是否直接抛错，默认为是
     * @throws
     * @return object
     */
    public function validate($data, $rules, $isEnd = true)
    {
        $validateResult = $this->validator->validate($data, $rules);
        if (!$validateResult && $isEnd) {
            $this->error(Error::PARAM_INVALIDATE, ($this->validator->getValidateErrors())[0]);
        }

        return $this->validator;
    }




}

<?php

namespace iUtils\yii2;

/**
 * 服务层基类
 * 
 * @author ray
 */
class Service extends \yii\base\BaseObject
{
    /**
     * 实例化一个服务实例
     * @param array $config
     * @return static
     */
    public static function newInstance($config = [])
    {
        return \yii::createObject(get_called_class(), $config);   
    }
}

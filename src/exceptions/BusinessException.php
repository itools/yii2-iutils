<?php

namespace iUtils\exceptions;

/**
 * 业务异常类,业务异常类可继承该类
 *
 * @author ray
 */
class BusinessException extends \Exception
{
    
}

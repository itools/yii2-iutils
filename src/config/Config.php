<?php

namespace iUtils\config;

use iUtils\yii2\Model;

/**
 * Created by PhpStorm.
 * User: zhangqing
 * Date: 2020-03-03
 * Time: 19:36
 */
class Config extends Model
{
    public static function tableName()
    {
        return 'config';
    }
}

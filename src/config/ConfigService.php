<?php

namespace iUtils\config;

use iUtils\Cacher as CacheService;

/**
 * 全局配置服务类
 *
 * @author ray
 */
class ConfigService
{
    /**
     * 配置读取类
     * @var ConfigAccessorInterface
     */
    private static $configAccessor = null;

    /**
     * 获取配置访问类
     * @return ConfigAccessorInterface
     * @throws
     */
    private static function getConfigAccessor()
    {
        if (is_null(static::$configAccessor)) {
            if (\Yii::$container->has('\iUtils\config\ConfigAccessorInterface')) {
                // 已经注入
                $configAccessor = \Yii::$container->get('\iUtils\config\ConfigAccessorInterface');
            } else {
                // 未注入自定义配置读取类，则使用iUtils内置的配置读取类
                $configAccessor = ConfigAccessor::newInstance();
            }

            static::$configAccessor = $configAccessor;
        }

        return static::$configAccessor;
    }

    /**
     * 获取全局配置,无配置返回false
     * @param string $key
     * @return string
     */
    public static function getValue($key)
    {
        $cacheKey = CacheService::getKey(CacheService::KEY_GLOBAL_CONFIG, $key);
        if (!CacheService::exists($cacheKey)) {
            $value = static::getConfigAccessor()->getValue($key);
            if ($value === false) {
                return false;
            }

            CacheService::set($cacheKey, $value, 10*60);
            return $value;
        }

        return CacheService::get($cacheKey);
    }

    /**
     * 获取json值的配置并转换为数组返回,无效的json字符串返回false
     * @param string $key
     * @return array
     */
    public static function getJsonValue($key)
    {
        $jsonValue = static::getValue($key);
        return json_decode($jsonValue, true);
    }

    /**
     * 更新配置
     * @param string $key
     * @param string|array|object $value
     */
    public static function updateValue($key, $value)
    {
        $value = is_array($value) ? \iUtils\helpers\StringHelper::jsonEncode($value) : $value;
        static::getConfigAccessor()->updateValue($key, $value);
        $cacheKey = CacheService::getKey(CacheService::KEY_GLOBAL_CONFIG, $key);
        CacheService::delete($cacheKey);
    }
}

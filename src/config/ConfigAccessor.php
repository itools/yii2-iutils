<?php

namespace iUtils\config;

/**
 * Created by PhpStorm.
 * User: zhangqing
 * Date: 2020-03-03
 * Time: 19:36
 */
class ConfigAccessor extends \iUtils\yii2\Service implements ConfigAccessorInterface
{
    /**
     * @var Config
     */
    public $configModel;

    public function __construct(Config $configModel, array $config = [])
    {
        $this->configModel = $configModel;

        parent::__construct($config);
    }

    /**
     * 获取全局配置
     * @param string $key
     * @return mixed
     */
    public function getValue($key)
    {
        return $this->configModel->scalar(['key' => $key], 'value', false);
    }

    /**
     * 更新全局配置
     * @param string $key
     * @param mixed $value
     * @return mixed
     */
    public function updateValue($key, $value)
    {
        $this->configModel->update(['value' => $value], ['key' => $key]);
    }
}

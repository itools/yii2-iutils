<?php

namespace iUtils\config;

/**
 * Created by PhpStorm.
 * User: zhangqing
 * Date: 2020-03-03
 * Time: 22:30
 */
interface ConfigAccessorInterface
{
    /**
     * 获取全局配置
     * @param string $key
     * @return mixed
     */
    public function getValue($key);

    /**
     * 更新全局配置
     * @param string $key
     * @param mixed $value
     * @return mixed
     */
    public function updateValue($key, $value);
}

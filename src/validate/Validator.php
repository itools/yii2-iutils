<?php

namespace iUtils\validate;

use Yii;
use ArrayObject;
use iUtils\Error;
use yii\validators\Validator as BaseValidator;

/**
 * Created by PhpStorm.
 * User: zhangqing
 * Date: 2020-01-06
 * Time: 14:57
 */
class Validator extends \iUtils\yii2\Service
{
    use ValidatorTrait;
    /**
     * 参数校验model类
     * @var \iUtils\validate\ValidatorModel
     */
    private $model;

    /**
     * 原始数据
     * @var array
     */
    private $data = [];

    /**
     * 需要校验的参数数组
     * @var array
     */
    private $needCheckParams = [];

    /**
     * 过滤后数据
     * @var array
     */
    private $filterData = [];

    /**
     * 默认验证器默认值
     */
    const DEFAULT_VALUE = null;

    /**
     * 自定义校验类
     * @var array
     */
    private static $customValidators = [
        'mobile' => 'iUtils\validate\validators\MobileValidator',// 手机号码
        'uuid' => 'iUtils\validate\validators\UuidValidator',// 36位uuid
        'idCard' => 'iUtils\validate\validators\IdCardValidator',// 居民身份证
    ];

    public function __construct(ValidatorModel $validatorModel, array $config = [])
    {
        $this->model = $validatorModel;

        parent::__construct($config);
    }

    /**
     * 获取自定义参数校验类配置
     * @param string $validateType 参数校验类型
     * @return mixed
     * @throws
     */
    private function getValidatorConfig($validateType)
    {
        // YII内置
        if (!empty(BaseValidator::$builtInValidators[$validateType])) {
            return BaseValidator::$builtInValidators[$validateType];
        }

        // 自定义mobile
        if (!empty(self::$customValidators[$validateType])) {
            return self::$customValidators[$validateType];
        }

        return $validateType;
    }

    /**
     * 组装参数校验类
     * @param string|array $validateType 参数校验类型
     * @param array $attributes 参数校验器属性
     * @return mixed
     * @throws
     */
    private function getValidateObject($validateType, $attributes)
    {
        if (is_array($validateType)) {
            $attributes = array_merge($validateType, $attributes);
        } else {
            $attributes['class'] = $validateType;
        }
        return Yii::createObject($attributes);
    }

    /**
     * 参数校验
     * @param array $data 数据项
     * @param array $rules 验证规则
     * @return bool
     * @throws
     */
    public function validate($data, $rules)
    {
        $this->data = $data;
        $validators = new ArrayObject();
        foreach ($rules as $rule) {
            if ($rule instanceof BaseValidator) {
                $validators->append($rule);
                continue;
            }

            if (!is_array($rule) || !isset($rule[0], $rule[1])) {
                throw new \Exception('Invalid validation rule: a rule must specify both attribute names and validator type.');
            }

            // 前两个值对应要校验的属性和校验类型
            $validateAttributes = $rule[0];
            $validateType = $rule[1];
            $otherAttrs = array_slice($rule, 2);

            // 校验参数为数组
            if (is_array($validateAttributes)) {
                foreach ($validateAttributes as $key => $value) {
                    // 验证提示信息为用户自定义
                    $valueOtherAttrs = $otherAttrs;
                    if (!is_int($key)) {
                        $valueOtherAttrs['message'] = $value;
                        $valueOtherAttrs['attributes'] = $key;
                        $this->getNeedCheckParams($key, $validateType, $valueOtherAttrs);
                    } else {
                        $valueOtherAttrs['attributes'] = $value;
                        $this->getNeedCheckParams($value, $validateType, $valueOtherAttrs);

                    }
                    unset($validateAttributes[$key]);
                }
            }

            if (!empty($validateAttributes)) {
                $otherAttrs['attributes'] = $validateAttributes;
                $this->getNeedCheckParams($validateAttributes, $validateType, $otherAttrs);
            }
        }

        // 对需要验证的参数加入过滤器
        foreach ($this->needCheckParams as $index => $item) {
            $key = $item['key'];
            if (isset($item['value'])) {
                $this->model->$key = $item['value'];
            }

            // 获取YII内置或者自定义的参数校验器
            $validateType = $this->getValidatorConfig($item['validate_type_alias']);
            $validators->append($this->getValidateObject($validateType, $item['otherAttrs']));
        }

        $this->model->setRules($validators);

        // 进行验证
        return $this->model->validate();
    }

    /**
     *  获取需要验证的参数数据（预处理）
     * @param string $key
     * @param string $validateTypeAlias
     * @param array $otherAttrs
     */
    private function getNeedCheckParams($key, $validateTypeAlias, $otherAttrs)
    {
        $data = $this->data;// 源数据
        $isParamOne = true;// 假设传入的是字符串或一维数组
        $paramOne = [
            'key' => $key,
            'validate_type_alias' => $validateTypeAlias,
            'otherAttrs' => $otherAttrs,
            'value' => self::DEFAULT_VALUE
        ];

        // $key可能为：key(字符串)、key.title(一维数组)、key.*.title(二维数组)
        $keyArr = explode('.', $key);
        if (count($keyArr) === 1) { // 值为字符串
            $this->filterData[$key] = self::DEFAULT_VALUE;
            if (isset($data[$key])) {
                $paramOne['value'] = $data[$key];
                $this->filterData[$key] = $data[$key];
            } elseif ($validateTypeAlias === 'default' && isset($otherAttrs['value'])) {
                $paramOne['value'] = $otherAttrs['value'];
                $this->filterData[$key] = $otherAttrs['value'];
            }
        } elseif (count($keyArr) > 1) {
            if ($keyArr[1] !== '*') { // 值为一维数组
                $this->filterData[$keyArr[0]][$keyArr[1]] = self::DEFAULT_VALUE;
                if (isset($data[$keyArr[0]][$keyArr[1]])) {
                    $paramOne['value'] = $data[$keyArr[0]][$keyArr[1]];
                    $this->filterData[$keyArr[0]][$keyArr[1]] = $data[$keyArr[0]][$keyArr[1]];
                } elseif ($validateTypeAlias === 'default' && isset($otherAttrs['value'])) {
                    $paramOne['value'] = $otherAttrs['value'];
                    $this->filterData[$keyArr[0]][$keyArr[1]] = $otherAttrs['value'];
                }
            } else { // 值为二维数组
                if (isset($data[$keyArr[0]])) {
                    foreach ($data[$keyArr[0]] as $subIndex => $subValue) {
                        // 设定初始值
                        $isParamOne = false;
                        $otherAttrs['attributes'] = $keyArr[0] . $subIndex . $keyArr[2];
                        $paramMulti = [
                            'key' => $keyArr[0] . $subIndex . $keyArr[2],
                            'validate_type_alias' => $validateTypeAlias,
                            'otherAttrs' => $otherAttrs,
                            'value' => self::DEFAULT_VALUE
                        ];
                        $this->filterData[$keyArr[0]][$subIndex][$keyArr[2]] = self::DEFAULT_VALUE;

                        // 数据存在则加入待验证数组
                        if (isset($subValue[$keyArr[2]])) {
                            $paramMulti['value'] = $subValue[$keyArr[2]];
                            $this->filterData[$keyArr[0]][$subIndex][$keyArr[2]] = $subValue[$keyArr[2]];
                        } elseif ($validateTypeAlias === 'default' && isset($otherAttrs['value'])) {
                            $paramMulti['value'] = $otherAttrs['value'];
                            $this->filterData[$keyArr[0]][$subIndex][$keyArr[2]] = $otherAttrs['value'];
                        }
                        // 打入需要校验的参数数组
                        $this->needCheckParams[] = $paramMulti;
                    }
                } else {
                    // 不存在数据则赋默认值
                    $this->filterData[$keyArr[0]][0][$keyArr[2]] = self::DEFAULT_VALUE;
                    if ($validateTypeAlias === 'default' && isset($otherAttrs['value'])) {
                        $paramOne['value'] = $otherAttrs['value'];
                        $this->filterData[$keyArr[0]][0][$keyArr[2]] = $otherAttrs['value'];
                    }
                }
            }
        }

        // 打入需要校验的参数数组
        if ($isParamOne) {
            $this->needCheckParams[] = $paramOne;
        }
    }

    /**
     * 获取有效的参数
     * @param string|array $selects
     * @return array
     */
    public function get($selects = '*')
    {
        // 返回全部字段
        $attributes = $this->filterData;
        if ($selects == '*') {
            return $attributes;
        }

        // 返回部分字段
        if (!is_array($selects)) {
            // 将字符串解析出数组格式；"A,  B, C,D" => ['A', 'B', 'C', 'D']
            $selects = preg_split('/\s*,\s*/', trim($selects), -1, PREG_SPLIT_NO_EMPTY);
        }

        foreach ($attributes as $key => $value) {
            if (!in_array($key, $selects)) {
                unset($attributes[$key]);
            }
        }

        return $attributes;
    }

    /**
     * 获取校验错误信息
     * @return array
     */
    public function getValidateErrors()
    {
        return $this->model->getErrorSummary(true);
    }

    /**
     * 验证数据是否有错误信息
     * @return bool
     */
    public function hasError()
    {
        if (!empty($this->getValidateErrors())) {
            return true;
        }
        return false;
    }

    /**
     * 错误时输出,不需要加return
     * @param string $customErrMsg 自定义错误信息
     * @param \Exception $exception 异常对象
     * @param array $data 返回数据
     * @return array
     */
    public function throw($customErrMsg = '', $data = [], $exception = null)
    {
        list($errMsg, $errCode) = explode('|', Error::PARAM_INVALIDATE);
        $response = [
            'errcode' => intval($errCode),
            'errmsg' => $errMsg . ($customErrMsg ? "({$customErrMsg})" : "({$this->getValidateErrors()[0]})"),
            'data' => $data
        ];

        if (YII_DEBUG && !is_null($exception)) {
            $response['exception'] = $exception;
        }

        \Yii::$app->response->data = $response;
        \Yii::$app->end();
    }
}

<?php

namespace iUtils\validate\validators;

use yii\validators\RegularExpressionValidator;

/**
 * UuidValidator
 * 验证36位UUID
 */
class UuidValidator extends RegularExpressionValidator
{
    /**
     * @inheritdoc
     */
    public $pattern = '/^[0-9A-F]{8}-[0-9A-F]{4}-[12345][0-9A-F]{3}-[0-9A-F]{4}-[0-9A-F]{12}$/i';
}
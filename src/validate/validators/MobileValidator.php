<?php

namespace iUtils\validate\validators;

use yii\validators\RegularExpressionValidator;

/**
 * Created by PhpStorm.
 * User: zhangqing
 * Date: 2020-02-27
 * Time: 14:26
 */
class MobileValidator extends RegularExpressionValidator
{
    /**
     * @var string 手机号码默认正则
     */
    public $pattern = '/^1[34578]\d{9}$/';
}

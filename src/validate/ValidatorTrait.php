<?php

namespace iUtils\validate;

/**
 * 验证器
 *
 * @author ray
 */
trait ValidatorTrait
{
    /**
     * 日期时间格式检查(5.3以上版本支持), true合法
     * @param $datetime string
     * @return bool
     */
    public static function isDateTime($datetime)
    {
        $format = 'Y-m-d H:i:s';
        $d = \DateTime::createFromFormat($format, $datetime);
        return $d && $d->format($format) == $datetime;
    }
    
    /**
     * 日期格式检查(5.3以上版本支持), true合法
     * @param $date string
     * @return bool
     */
    public static function isDate($date)
    {
        $format = 'Y-m-d';
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
    
    /**
     * 检查字符串是否base64编码
     * @param string $str
     * @return bool
     */
    public static function isBase64($str)
    {
        return base64_encode($str) === false ? false : true;
    }
    
    /**
     * 检查字符串是否是UTF8编码
     * @param string $str 字符串
     * @return Boolean
     */
    public static function isUtf8($str)
    {
        $c = 0;
        $b = 0;
        $bits = 0;
        $len = strlen($str);
        for ($i = 0; $i < $len; $i++) {
            $c = ord($str[$i]);
            if ($c > 128) {
                if (($c >= 254))
                    return false;
                elseif ($c >= 252)
                    $bits = 6;
                elseif ($c >= 248)
                    $bits = 5;
                elseif ($c >= 240)
                    $bits = 4;
                elseif ($c >= 224)
                    $bits = 3;
                elseif ($c >= 192)
                    $bits = 2;
                else
                    return false;
                if (($i + $bits) > $len)
                    return false;
                while ($bits > 1) {
                    $i++;
                    $b = ord($str[$i]);
                    if ($b < 128 || $b > 191)
                        return false;
                    $bits--;
                }
            }
        }
        
        return true;
    }
    
    /**
     * Finds whether a string is a valid email address.
     */
    public static function isEmail(string $value)
    {
        $atom = "[-a-z0-9!#$%&'*+/=?^_`{|}~]"; // RFC 5322 unquoted characters in local-part
        $alpha = "a-z\x80-\xFF"; // superset of IDN
        return (bool) preg_match("(^
                (\"([ !#-[\\]-~]*|\\\\[ -~])+\"|$atom+(\\.$atom+)*)
                @
                ([0-9$alpha]([-0-9$alpha]{0,61}[0-9$alpha])?\\.)+   
                [$alpha]([-0-9$alpha]{0,17}[$alpha])?               
        \\z)ix", $value);
    }
        
    /**
     * Finds whether a string is a valid http(s) URL.
     */
    public static function isUrl(string $value)
    {
        $alpha = "a-z\x80-\xFF";
        return (bool) preg_match("(^
                https?://(
                        (([-_0-9$alpha]+\\.)*                       
                                [0-9$alpha]([-0-9$alpha]{0,61}[0-9$alpha])?\\.)?
                                [$alpha]([-0-9$alpha]{0,17}[$alpha])?
                        |\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}
                        |\[[0-9a-f:]{3,39}\]                      
                )(:\\d{1,5})?                                   
                (/\\S*)?                                        
        \\z)ix", $value);
    }

    /**
     * Finds whether a string is a valid URI according to RFC 1738.
     */
    public static function isUri(string $value)
    {
        return (bool) preg_match('#^[a-z\d+\.-]+:\S+\z#i', $value);
    }
    
    /**
     * 验证字符串复杂度是否满足要求,默认至少8位,至少有一个大写,一个小写,一个特殊字符,一位数字构成
     * @param string $value
     * @param int $len 最小长度, 0时不要求
     * @param int $lowers 最少包含的小写字母个数, 0时不要求
     * @param int $uppers 最少包含的大写字母个数, 0时不要求
     * @param int $specials 最少包含的特殊字符个数, 0时不要求
     * @param int $numbers 最少包含的数字个数, 0时不要求
     * @return boolean
     */
    public static function isSafeKey(string $value, $len = 8, $lowers = 1, $uppers = 1,  $specials = 1, $numbers = 1)
    {
        $r1 = '/[A-Z]/';  //Uppercase
        $r2 = '/[a-z]/';  //lowercase
        $r3 = '/[!@#$%^&*()\-_=+{};:,<.>]/';  // whatever you mean by 'special char'
        $r4 = '/[0-9]/';  //numbers

        if ($uppers && preg_match_all($r1, $value, $o) < $uppers) {
            return false;
        } 

        if ($lowers && preg_match_all($r2, $value, $o) < $lowers) {
           return false; 
        }
        
        if ($specials && preg_match_all($r3, $value, $o) < $specials) {
           return false; 
        }
        
        if ($numbers && preg_match_all($r4, $value, $o) < $numbers) {
           return false; 
        }
        
        if ($len && strlen($value) < $len) {
           return false; 
        }
        
        return true;
    }
}

<?php

namespace iUtils\validate;

/**
 * Created by PhpStorm.
 * User: zhangqing
 * Date: 2020-01-06
 * Time: 14:44
 */
class ValidatorModel extends \yii\base\Model
{
    /**
     * @var array 参数校验规则
     */
    private $_rules = [];

    /**
     * @var array 参数列表
     */
    private $_attributes = [];

    /**
     * 设置校验规则
     * @param $rules
     */
    public function setRules($rules)
    {
        $this->_rules = $rules;
    }

    /**
     * 获取校验规则
     * @return array
     */
    public function rules()
    {
        return $this->_rules;
    }

    /**
     * 获取属性列表
     * @return array
     */
    public function attributes()
    {
        return $this->_attributes;
    }

    /**
     * 获取属性值
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return isset($this->_attributes[$name]) ? $this->_attributes[$name] : null;
    }

    /**
     * 设置属性值
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->_attributes[$name] = $value;
    }
}
